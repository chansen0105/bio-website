<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $current ?> | Cody Hansen</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- FAVICON -->
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">

		<!-- CSS -->
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/below.css">

		<!-- JAVASCRIPT -->
		<script type="text/javascript" src="javascript/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="javascript/mainScript.js"></script>

		<!-- FONTS -->
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,700' rel='stylesheet' type='text/css'>

	</head>
	<body>

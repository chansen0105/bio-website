<header>
			<div id="headBackground">
				<a href="about">
					<div id="leftHead">
						<h1 id="name">Cody Hansen</h1>
						<h3>Software Developer</h3>
					</div>
					<h1 id="logoText">&lt;ch&gt;</h1>
				</a>
				<nav>
					<ul>
						<li><a href="about" <?php if ($current == "About") { echo 'class="current"'; } ?> >About</a></li>
						<li><a href="portfolio" <?php if ($current == "Portfolio") { echo 'class="current"'; } ?> >Portfolio</a></li>
						<li><a href="contact" <?php if ($current == "Contact") { echo 'class="current"'; } ?> >Contact</a></li>
					</ul>
				</nav>
			</div>
			<img src="pic/arrow.png" alt="Menu arrow" id="arrow">
		</header>
		<div id="center">
			<div id="main">

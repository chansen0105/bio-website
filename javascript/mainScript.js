$(document).ready(function() {
	navVisiblity();
	$("#main").css("display", "none");
	$("#main").fadeIn(500);

	$("#arrow").click(function() {
		$("nav ul").slideToggle();
	});

	$(window).resize(navVisiblity);
});

function navVisiblity() {
	if($(window).width() < 600) {
			$("nav ul").slideUp();
		} else {
			$("nav ul").slideDown();
		}
}

function clearMessage(textarea) {
	if ($(textarea).val() == "Type your e-mail here.") {
		$(textarea).empty();
	}
}

function validateForm() {

	var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

	$("td input, textarea").each(function() {
		$(this).css("border", "solid 1px #61a1ca");
	});

	var errors = new Array();
	if ($("#nameField").val().trim() == "") {
		errors.push("Please enter a valid name.");
		$("#nameField").css("border", "solid 2px #0b7fca");
	}
	if ($("#emailField").val().trim() == "" || !pattern.test($("#emailField").val().trim())) {
		errors.push("Please enter a valid e-mail.");
		$("#emailField").css("border", "solid 2px #0b7fca");
	}
	if ($("#messageArea").val().trim() == "" || $("#messageArea").val().trim() == "Type your e-mail here.") {
		errors.push("Please enter your message.");
		$("#messageArea").css("border", "solid 2px #0b7fca");
	}
	if (errors.length != 0) {
		$("#errorList").empty();
		$(errors).each( function() {
			$("#errorList").append("<li>" + this + "</li>");
		}
		);
		return false;
	}

}

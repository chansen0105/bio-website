<?php

	$current = "CHANGE THIS";

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$name = trim(filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING));
		$email = trim(filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL));
		$subject = trim(filter_input(INPUT_POST, "subject", FILTER_SANITIZE_STRING));
		$message = trim(filter_input(INPUT_POST, "message", FILTER_SANITIZE_SPECIAL_CHARS));

		$errors = array();
		$pattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

		if ($_POST["comment"] != "") {
			exit("Form input invalid.");
		}

		if ($name == "") {
			$errors[] = "Please enter a valid name.";
		}

		if ($email == "" || !preg_match($pattern, $email)) {
			$errors[] = "Please enter a valid e-mail.";
		}

		if ($subject == "") {
			$subject = "No Subject";
		}

		if ($message == "" || $message == "Type your e-mail here.") {
			$errors[] = "Please enter your message.";
		}

		if (empty($errors)) {
			$header = "From: " . $email;
			$to = "c.hansen0105@gmail.com";
			$message = $subject . "\n\n" . wordwrap($message,100);
			mail($to, "Website Message", $message, $header);
			header("location:?status=sent");
		}

  }

?>
<!-- INCLUDE SETUP -->
<?php include("include/setup.php"); ?>

		<!-- INCLUDE HEADER -->
		<?php include("include/header.php"); ?>

    <!-- CONTACT SENT -->
    <?php
    	if (isset($_GET["status"]) && $_GET["status"] == "sent") {
    ?>

    <h1>Thank you!</h1>
    <p>I'll get back to you as soon as I can.</p>

    <?php } else { ?>

    <h1 id="parallaxHeader">ABOUT</h1>
    <!-- ABOUT -->
    <div id="aboutMain" class="container">
			<img src="pic/me.jpg" alt="Picture of myself" id="myPic">
			<section id="left">
				<p>Hello! <br /> My name is Cody Hansen. <br /> I am a simple man with the firm belief that most problems can be solved using code. <br /> I define myself as an aspiring game developer, mobile wizard, and web virtuoso. <br /> It is difficult to find something that I don't find interest in and immediately try to learn.</p>
			</section>
			<section id="right">
				<p><a href="mailto:c.hansen0105@gmail.com" target="_blank">c.hansen0105&commat;gmail.com
				<img src="pic/logos/emailgrey.png" alt="Picture of an Email icon" class="sideIcons"></a> <br />
				<a href="http://twitter.com/leatherfur" target="_blank">&commat;Leatherfur
				<img src="pic/logos/twittergrey.png" alt="Twitter logo" class="sideIcons"></a> <br />
				<a href="https://www.linkedin.com/in/chansen0105" target="_blank">linkedin.com/in/chansen0105
				<img src="pic/logos/linkedingrey.png" alt="Linkedin logo" class="sideIcons"></a></p>
			</section>
		</div>

    <!-- PORTFOLIO -->
    <h1 id="parallaxHeader">PORTFOLIO</h1>
    <div id="porfolioMain" class="container">
      <h1>Currently Empty!</h1>
      <p>Check back soon!</p>
    </div>

    <!-- CONTACT -->

    <h1 id="parallaxHeader">CONTACT</h1>
    <div id="contactMain" class="container">
      <p>If you want to send a quick inquiry, fill out the form below and I will get back to you as soon as possible!</p>

      <ul id="errorList"></ul>

      <?php if (!empty($errors)) {

      	echo "<ul>";

      	foreach($errors as $error) {

      		echo "<li>" . $error . "</li>";

      	}

      	echo "</ul>";

      }

      ?>

    	<form method="post" action="index.php" onsubmit="return validateForm()">
    		<table>
    			<tr>
    				<th><label for="nameField">Name</label></th>
    				<td><input type="text" name="name" id="nameField" onclick="clearErrorColor(this)" value="<?php if(isset($name)) { echo $name; } ?>") /></td>
    			</tr>
    			<tr>
    				<th><label for="emailField">E-mail</label></th>
    				<td><input type="text" name="email" id="emailField" onclick="clearErrorColor(this)" value="<?php if(isset($email)) { echo $email; } ?>"/></td>
    			</tr>
    			<tr>
    				<th><label for="subjectField">Subject</label></th>
    				<td><input type="text" name="subject" id="subjectField" value="<?php if(isset($subject)) { echo $subject; } ?>"/></td>
    			</tr>
    			<tr>
    				<th><label for="messageArea">Message</label></th>
    				<td><textarea rows="6" name="message" id="messageArea" onclick="clearErrorColor(this)" onfocus="clearMessage(this);"><?php
    					if(isset($message) && $message != "") {
    						echo $message;
    					} else {
    						echo "Type your e-mail here.";
    					}?></textarea></td>
    			</tr>
    			<tr style="display:none">
    				<th><label for="commentArea">Comments</label></th>
    				<td><input type="text" name="comment" id="commentField" /></td>
    			</tr>
    		</table>
    		<input type="submit" value="Send E-mail" id="submitButton" />
    	</form>
    </div>

		<!-- INCLUDE FOOTER -->
		<?php } include("include/footer.php"); ?>

<?php

	$current = "About";

?>
<!-- INCLUDE SETUP -->
<?php include("include/setup.php"); ?>

		<!-- INCLUDE HEADER -->
		<?php include("include/header.php"); ?>

		<div id="aboutMain">
			<img src="pic/me.jpg" alt="Picture of myself" id="myPic">
			<section id="left">
				<p>Hello! <br /> My name is Cody Hansen. <br /> I am a simple man with the firm belief that most problems can be solved using code. <br /> I define myself as an aspiring game developer, mobile wizard, and web virtuoso. <br /> It is difficult to find something that I don't find interest in and immediately try to learn.</p>
			</section>
			<section id="right">
				<p><a href="mailto:c.hansen0105@gmail.com" target="_blank">c.hansen0105&commat;gmail.com
				<img src="pic/logos/emailgrey.png" alt="Picture of an Email icon" class="sideIcons"></a> <br />
				<a href="http://twitter.com/chansen0105" target="_blank">&commat;chansen0105
				<img src="pic/logos/twittergrey.png" alt="Twitter logo" class="sideIcons"></a> <br />
				<a href="https://teamtreehouse.com/codyhansen" target="_blank">teamtreehouse.com/codyhansen
				<img src="pic/logos/treehousegrey.png" alt="Treehouse logo" class="sideIcons"></a><br />
				<a href="https://www.linkedin.com/in/chansen0105" target="_blank">linkedin.com/in/chansen0105
				<img src="pic/logos/linkedingrey.png" alt="Linkedin logo" class="sideIcons"></a></p>
			</section>
		</div>

		<!-- INCLUDE FOOTER -->
		<?php include("include/footer.php"); ?>
